import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    deleteAuthor() {
      const author = this.get('model');
      author.destroyRecord().then(()=>{
        this.transitionToRoute('author.index');
      });
    }
  }
});
