import DS from 'ember-data';

export default DS.Model.extend({
    title: DS.attr(),
    genre: DS.attr(),
    language: DS.attr(),
    author: DS.hasMany('author')
});
