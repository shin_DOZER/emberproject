import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
    host: 'http://localhost:4080',
    namespace: 'api',
    pathForType(type) {
        return type;
    }
});
