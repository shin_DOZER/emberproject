import DS from 'ember-data';

export default DS.JSONAPISerializer.extend({
  serialize(snapshot, options) {
    const json = this._super(snapshot, options);
    // FIX ME PLEASE!!!
    json.data.type = snapshot.modelName;
    return json;
  }
});
